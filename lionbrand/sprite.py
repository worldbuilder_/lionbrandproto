import pygame
from pygame.sprite import Sprite
import group

class BaseSprite(Sprite):
    """This is the base sprite class."""
    def __init__(self, size):
        Sprite.__init__(self)
        self.image = pygame.Surface(size)
        self.rect = self.image.get_rect()


class ButtonSprite(Sprite):
    pass
