import pygame
import pygame.gfxdraw
from pygame.locals import *


Screen_Width = 800
Screen_Height = 600
Screen = pygame.display.set_mode([Screen_Width, Screen_Height] )


def makeScreenItems(screenManager):
    objectSurface = pygame.Surface((40, 40))
    objectSurface.fill((255, 0, 0))

    screenManager.add_screen_object(objectSurface, (0, 0))
    screenManager.add_screen_object(objectSurface, (0, 50))
    screenManager.add_screen_object(objectSurface, (50, 0))
    screenManager.add_screen_object(objectSurface, (50, 50))
    screenManager.add_screen_object(objectSurface, (100, 50))
    return screenManager

def shutDown():
    pygame.display.quit()

class Manager(object):
    def __init__(self, ):
	self.x = 20

class ScreenManager(Manager):
	
    def __init__(self, screen):
	Manager.__init__(self,)
	self.current_screen_render_index = 0
        self.current_position = 0
        #screen display
        self.screen_object = screen
        self.render_list = []
        self.render_list_id = 0
        self.position = []	#Holds XY positions of 
        self.position_index = 0

    def make_screen_rendering_list(self):
        self.render_list.append([])
	self.current_render_index = self.current_screen_render_index
        self.current_screen_render_index += 1
		
    def make_position_list(self):
        self.position.append([])
        self.current_position_index = self.position_index
        self.position_index += 1

    def add_screen_object(self, screen_object, position):
        self.render_list[self.current_render_index].append(screen_object)
        self.position[self.current_position_index].append(position)
			

    def is_render_list_out_of_range(self, render_list, position_number ):
	if position_number > len(self.render_list):
	    return True
	else:
	    return False

    def update_screen(self, ):
	self.screen_object.fill((255, 255, 255))
	pygame.gfxdraw.rectangle(self.screen_object, self.screen_object.get_rect(), (255, 0, 0))
	for rendered_object_list in self.render_list:
	    r_obj_and_pos = zip(rendered_object_list, self.position[0])
            #print rObjAndPos
	    for r_obj in r_obj_and_pos:
		self.screen_object.blit(r_obj[0], r_obj[1])
				
	pygame.display.update()

    

def updateGame(SM):

    while True:
	SM.update_screen()
	for e in pygame.event.get():
            if e.type == KEYUP and K_q:
		    pygame.quit()



if __name__ == '__main__':
    pygame.init()
    screenManager = ScreenManager(Screen)
    screenManager.make_screen_rendering_list()
    screenManager.make_position_list()
    screenManager = makeScreenItems(screenManager)
    updateGame(screenManager)
    shutDown()




