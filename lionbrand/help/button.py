
import pygame
pygame.font.init()
pygame.init()
font = pygame.font.SysFont("ubuntu", 14)
display = pygame.display.set_mode((1366, 768))

TILERENDERLIST = 'TILERENDERLIST'
SPRITERENDERLIST = 'SPRITERENDERLIST'


class Manager(object):
    def __init__(self, ):
        print ''

class RLO(object):
   """This is an render list object. They have position and image of sprite."""
   def __init__(self, image, posisition):
       object.__init__(self)
       self.image_sprite = image
       self.pos = position

class RenderList(object):
    """This object holds surfaces/sprites to be rendered."""
    #render list list
    rll = []
    def __init__(self, name):
        self.list_of_objects = []
        self.name_of_render_list = name

        RenderList.rll.append(self)
    def add_object(self, *rlos):
        for rlo in rlos:
            self.list_of_objects.append(rlo)

    def return_index(self, sprite):
        return self.list_of_objects.index(sprite)

    def insert(self, sprite, pos):
        self.list_of_objects.insert(pos, sprite)

    @staticmethod
    def returnlist(name):
        for rl in RenderList.rll:
            if name == rl.name_of_render_list:
                return rl



class ScreenManager(Manager):
	
    def __init__(self, screen):
	Manager.__init__(self,)
	self.current_screen_render_index = 0
        self.current_position = 0
        #screen display
        self.screen_object = screen
        self.render_list = []
    

    def add_render_list(self, render_list):
         self.render_list.append(render_list)

    #This is now obsolete.
    def make_screen_rendering_list(self):
        self.render_list.append([])
	self.current_render_index = self.current_screen_render_index
        self.current_screen_render_index += 1
    #This is now obsolete.	
    def make_position_list(self):
        self.position.append([])
        self.current_position_index = self.position_index
        self.position_index += 1

    #This is now obsolete.
    def add_screen_object(self, screen_object, position):
        self.render_list[self.current_render_index].append(screen_object)
        self.position[self.current_position_index].append(position)
			

    def is_render_list_out_of_range(self, render_list, position_number ):
	if position_number > len(self.render_list):
	    return True
	else:
	    return False

    def update_screen(self, ):
	for rendered_object_list in self.render_list:
	    r_obj_and_pos = zip(rendered_object_list, self.position[0])
            #print rObjAndPos
	    for r_obj in r_obj_and_pos:
		self.screen_object.blit(r_obj[0], r_obj[1])
				
	pygame.display.update()

class ButtonSprite(pygame.sprite.Sprite):
    def __init__(self, surface):
        pygame.sprite.Sprite.__init__(self)
        self.image = surface
        self.rect = self.image.get_rect()


class Button(object):
    display = display

    ubuntu_font = font
    characterheight = ubuntu_font.size("a")[1]
    button_size = 70, characterheight

    currentone = None

    buttoncolor = pygame.Color(0, 255, 50)
    buttoncolorhover = pygame.Color(0, 255, 50, 50)
    buttoncolorpressed = pygame.Color(0, 255, 50, 120)
    
    """This is the class that makes buttons for the Navigator Screen."""

    def __init__(self, *texts):
        object.__init__(self)
        
        self.textsizecount = 0
        self.font = Button.ubuntu_font
        self.texts = texts
        #don't know why i need bottom for loop. Looks like I already have chosen dimensions for text size.
        for a in texts:
            self.textsizecount = self.textsizecount + 1


        self.button_size = self.make_button_size(self.textsizecount)

        self.button_unpressed, self.button_pressed, self.button_hover = self.make_button(self.button_size)

        self.font.set_bold(True)

        self.buttons_together = self.button_unpressed, self.button_pressed, self.button_hover

        self.paste_text(*texts)



    #methods
    def make_button_size(self, count):
        button_size = []
        button_size.append(self.button_size[0])
        button_size.append(self.button_size[1] * count)
        return button_size

    @staticmethod
    def set_current_one( thisone ):
        Button.currentone = thisone
    
    def watch(self, ):
        if self.inside_rect(pygame.mouse.get_pos()):
            self.change_to_hover()
        else:
            self.change_to_not_hover()
        if clicked_on_rect(event):
            self.change_to_clicked()

    def change_to_not_hover():
        pass
    def change_to_clicked():
        pass

    def change_to_hover():
        pass

    def inside_rect(self, mospos):
        if self.button_rect.collide_point(mospos):
            return True
        else:
            return False

    def make_button(self, button_size):
        surf_pressed = ButtonSprite(pygame.Surface(button_size))
        surf_pressed.image.convert_alpha() 
        surf_pressed.image.fill(Button.buttoncolorpressed)

        surf_hover = ButtonSprite(pygame.Surface(button_size))
        surf_hover.image.convert_alpha()
        surf_hover.image.fill(Button.buttoncolorhover)

        surf_unpressed = ButtonSprite(pygame.Surface(button_size))
        surf_unpressed.image.fill(Button.buttoncolor)
        return surf_unpressed, surf_pressed, surf_hover

    def paste_text(self, *texts):
        if len(texts) > 1:
            for i in range(len(texts)):
                currenttextsurface = Button.ubuntu_font.render(texts[i], True, (0, 0, 255))
                self.button_unpressed.image.blit(currenttextsurface, (0, i * Button.characterheight))
                self.button_pressed.image.blit(currenttextsurface, (0, i* Button.characterheight))
                self.button_hover.image.blit(currenttextsurface, (0, i* Button.characterheight))
        else:
            currenttextsurface = Button.ubuntu_font.render(texts[0], 1, pygame.Color(0, 0, 255))
            self.button_unpressed.image.blit(currenttextsurface, (0, 0))
            self.button_pressed.image.blit(currenttextsurface, (0, 0))
            self.button_hover.image.blit(currenttextsurface, (0, 0))

        self.button_pressed


