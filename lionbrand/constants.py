#!/usr/bin/env python
import pygame
pygame.init()

"""This is the constants file. Contains all definitions for things that are to stay the same over the runtime of the program."""

SCREENWIDTH = 1366
SCREENHEIGHT = 768
SCREENSIZE = SCREENWIDTH, SCREENHEIGHT

screen = pygame.display.set_mode(SCREENSIZE)

whole_map = 4032, 3008
whole_map_center = whole_map[0]/2, whole_map[1]/2

white_transparent = (255, 255, 255, 50)
white = (255, 255, 255)
black = (0, 0, 0)


