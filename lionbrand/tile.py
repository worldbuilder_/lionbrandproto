import pygame
import pygame.sprite
from sprite import BaseSprite

tile_size = 64, 64

class TileCollective(pygame.sprite.Group):
    """This is the group that will hold all tiles. This collective will be used to recieve all tiles for all purposes."""
    def __init__(self, ):
        pygame.sprite.Group.__init__(self, )

tile_collective = TileCollective()
class Tile(BaseSprite):
    """This is the tiles."""
    collective = []
    def __init__(self, size):
        BaseSprite.__init__(self, size)
        Tile.collective.append(self)
        self.add(tile_collective)
        
    

