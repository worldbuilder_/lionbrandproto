#!/usr/bin/env python

import pygame
from ts import system

class CollidingSystem(object):
    """This is the collision detection system."""
    Collidable_Objects = []
    def __init__(self, system, npcs, hero, etc):
        self.system, self.npcs, self.hero, self.etc = system, npcs, hero, etc
    def add_collidable_object( *args):
        for i in args:
            CollidingSystem.Collidable_Objects.append(i)

class SurroundingArea(object):
    """This is for moving sprites. This is the surrounding area that will be monitored for collision detection."""
    def __init__(self, etc):
        self.moving_object = etc
        
    def update(self, ):

