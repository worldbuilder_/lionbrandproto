import pygame
import ts
import font_sys
from tile import tile
from sprite import ButtonSprite

WHITETRANSPARENT = pygame.Color(255, 255, 255, 50)
WHITE = pygame.Color(255, 255, 255)
BLACK = pygame.Color(0, 0, 0)

pygame.init()
whole_map = 4032, 3008
whole_map_center = whole_map[0]/2, whole_map[1]/2

display = pygame.display.set_mode((1366, 768))
display_rect = display.get_rect()

#pygame.Rect(left, top, width, height)
#pygame.Rect((left, top), (width, height))
palette_screen_rect = pygame.Rect(display_rect.w - ( display_rect.w * .25 + .5), 0, display_rect.w * .25 + .5 , display_rect.h)  
palette_screen = pygame.Surface(palette_screen_rect.size)

#this is the field to paste and make a map of tiles
screen_field = 768, 768

#initializes tiling system 
ts.init()
ts.ts

class Field(object):
    """this is the field that you paste tiles on."""
#used to just draw all the rects
fixed_sprite_group = pygame.sprite.Group()

class Button(ButtonSprite):
    """This class is for the buttons of the navigator. These buttons will have up to three different instances of this class that will be in groups that represent hover nonhover and pressed unpressed."""
    def __init__(self, ):
        pass


class Button(object):
    display = display
    ubuntu_font = font_sys.font 
    button_size = 70, 30 
    button_color = pygame.Color(0, 255, 50)
    button_color_hover = pygame.Color(0, 255, 50, 50)
    button_color_pressed = pygame.Color(255, 255, 255)
    """This is the class that makes buttons for the Navigator Screen."""
    def __init__(self, *text, function = None):
        object.__init__(self)
        self.text_lines_size = []
        font = Button.ubuntu_font
        for a in text:
            self.text_lines_size.append(font.size(a))
        self.button_size = self.make_button_size(*self.text_lines_size)
        self.button_unpressed, self.button_pressed, self.button_hover = self.make_button(self.button_size)

    def make_button_size(self, *text_size):
        button_size = Button.button_size
        textcount = len(text_size)
        button_size[1] = button_size[1] * textcount 
        return button_size

    def make_button(self, button_size):
        surf_pressed = pygame.Surface(button_size)
        surf_pressed.fill(button_color_pressed

        surf_hover = pygame.Surface(button_size)
        surf_hover.convert_alpha()
        surf_hover.fill(Button.button_color_hover)

        surf_unpressed = pygame.Surface(button_size)
        surf_unpressed.fill(Button.button_color)
        return surf_unpressed, surf_pressed, surf_hover

    def paste_text(self, *texts, font):
        font.set_bold()


        
    def load_functionality(self, function):
        self.function = function
    @staticmethod
    def update():
        pass

class Navigator(object):
    """This is the screen section that chooses options."""
    def __init__(self, ):
        object.__init__(self, display)
        self.make_background(display)
        self.buttons = []

    def make_background(self, display):
        self.bg = pygame.Surface((display.get_rect().w * .25 + .5, display.get_rect().h))
    def add_button(button):
        self.buttons.append(button)
        self.update_button(len(self.buttons))

    def update_button(self, index ):
        """possibly won't need this because the button instances already hold a rect with its coordinates."""
        pass
